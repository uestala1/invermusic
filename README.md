# Invermusic

## Especificaciones

+ Python 3.6
+ Django 1.11.2
+ Postgres 9.6.3

## Iniciar el proyecto

Introducir en la terminal el siguiente comando, una vez se haya navegado a la carpeta raíz del proyecto:

+ python manage.py runserver

## Desarrollo realizado

+ Entidades principales de la base de datos:

++ Usuarios
++ Artistas
++ ArtistasVideos
++ ArtistasRecaudación

+ CRUD de las entidades principales
+ Listado y ficha de artistas
+ Inicio de sesión, registro del usuario y roles
+ BackOffice (el que ofrece Django) para gestionar Usuarios
+ Menú principal, secundario y regiones de la plantilla
+ Especificación Funcional (en PDF)

## Pendiente de desarrollo

+ Módulos de la sección de la derecha
+ Buscador
+ Pasarela de pago
+ CRUD de las entidades principales en el BackOffice, para la gestión de lo administradores
+ Mejorar la interfaz del usuario
+ Testear
+ Calendario (segunda fase del proyecto)