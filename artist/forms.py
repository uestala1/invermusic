from django import forms

from .models import Artist
from .models import ArtistVideo
from .models import ArtistCollect

class ArtistForm(forms.ModelForm):
	class Meta:
		model = Artist
		fields = ['name', 'email', 'picture', 'description', 'genre', 'create']

class ArtistVideoForm(forms.ModelForm):
	class Meta:
		model = ArtistVideo
		fields = ['url', 'order']

class ArtistCollectForm(forms.ModelForm):
	class Meta:
		model = ArtistCollect
		fields = ['name', 'description', 'total', 'date']