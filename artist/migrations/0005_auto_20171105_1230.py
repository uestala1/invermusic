# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-11-05 12:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('artist', '0004_auto_20171105_1043'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artistvideo',
            name='create',
            field=models.DateField(verbose_name='date published'),
        ),
    ]
