from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Artist(models.Model):
    POP 	= 'PO'
    INDIE 	= 'IN'
    ROCK	= 'RO'
    HEAVY   = 'HE'
    HIPHOP  = 'HH'
    RAP     = 'RA'
    SOUL 	= 'SO'
    JAZZ	= 'JA'
    FUNK    = 'FU'
    REAGGE	= 'RE'
    REATON  = 'RT'
    TRAP    = 'TR'
    ELEC	= 'EL'
    CLASS   = 'CL'
    EXPER   = 'EX'
    TRAD    = 'TD'
    OTHER   = 'OT'
    GENRES_CHOICES = (
        (POP, 		'Pop'),
        (INDIE, 	'Indie'),
        (ROCK, 		'Rock'),
        (HEAVY,     'Heavy Metal'),
        (HIPHOP,    'Hip hop'),
        (RAP,       'Rap'),
        (SOUL, 		'Soul'),
        (JAZZ, 		'Jazz'),
        (FUNK,      'Funk'),
        (REAGGE, 	'Reagge'),
        (REATON,    'Reaggeton'),
        (TRAP,      'Trap'),
        (ELEC, 		'Electronic'),
        (CLASS,     'Clásica'),
        (EXPER,     'Experimental'),
        (TRAD,      'Tradicional'),
        (OTHER,     'Other'),
    )

    user        = models.OneToOneField(User, on_delete=models.CASCADE)
    name 		= models.CharField(max_length=255)
    email 		= models.EmailField(max_length=255)
    web         = models.CharField(max_length=255, null=True)
    phone       = models.CharField(max_length=15, null=True)
    contact     = models.CharField(max_length=255, null=True)
    description = models.TextField()
    genre 		= models.CharField(
        max_length	= 2,
        choices 	= GENRES_CHOICES,
        default 	= INDIE
    )
    picture     = models.ImageField(upload_to='media/artist/pictures', null=True, blank=True)
    create 		= models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % (self.name)

class ArtistVideo(models.Model):
    url         = models.CharField(max_length=255)
    order       = models.IntegerField()
    create      = models.DateField(auto_now=True)
    artist      = models.ForeignKey(Artist, on_delete=models.CASCADE)

    def __str__(self):
        return self.url

    class Meta:
        ordering = ('order',)

class ArtistCollect(models.Model):
    name        = models.CharField(max_length=255)
    description = models.TextField()
    total       = models.DecimalField(max_digits=7, decimal_places=2)
    date        = models.DateField()
    create      = models.DateField(auto_now=True)
    artist      = models.ForeignKey(Artist, on_delete=models.CASCADE)
    closed      = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('date',)