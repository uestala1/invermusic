from django.conf.urls import url
from . import views

from .views import (
    ArtistList,
    ArtistDetail,
    ArtistCreate,
    ArtistUpdate,
    ArtistDelete,
    ArtistVideoList,
    ArtistVideoDetail,
    ArtistVideoCreate,
    ArtistVideoUpdate,
    ArtistVideoDelete,
    ArtistCollectList,
    ArtistCollectDetail,
    ArtistCollectCreate,
    ArtistCollectUpdate,
    ArtistCollectDelete
)

urlpatterns = [

    url(r'^signup/$', views.signup, name='signup'),

    url(r'^$', ArtistList.as_view(), name='list'),
    url(r'^(?P<pk>\d+)$', ArtistDetail.as_view(), name='detail'),
    url(r'^new$', ArtistCreate.as_view(), name='new'),
    url(r'^edit/(?P<pk>\d+)$', ArtistUpdate.as_view(), name='edit'),
    url(r'^delete/(?P<pk>\d+)$', ArtistDelete.as_view(), name='delete'),

    url(r'^video/(?P<fk>\d+)/$', ArtistVideoList.as_view(), name='video_list'),
    url(r'^video/(?P<fk>\d+)/(?P<pk>\d+)$', ArtistVideoDetail.as_view(), name='video_detail'),
    url(r'^video/(?P<fk>\d+)/new$', ArtistVideoCreate.as_view(), name='video_new'),
    url(r'^video/(?P<fk>\d+)/edit/(?P<pk>\d+)$', ArtistVideoUpdate.as_view(), name='video_edit'),
    url(r'^video/(?P<fk>\d+)/delete/(?P<pk>\d+)$', ArtistVideoDelete.as_view(), name='video_delete'),

    url(r'^collect/(?P<fk>\d+)/$', ArtistCollectList.as_view(), name='collect_list'),
    url(r'^collect/(?P<fk>\d+)/(?P<pk>\d+)$', ArtistCollectDetail.as_view(), name='collect_detail'),
    url(r'^collect/(?P<fk>\d+)/new$', ArtistCollectCreate.as_view(), name='collect_new'),
    url(r'^collect/(?P<fk>\d+)/edit/(?P<pk>\d+)$', ArtistCollectUpdate.as_view(), name='collect_edit'),
    url(r'^collect/(?P<fk>\d+)/delete/(?P<pk>\d+)$', ArtistCollectDelete.as_view(), name='collect_delete'),

]