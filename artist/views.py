from django.core.urlresolvers import reverse_lazy

from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)

from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from django.shortcuts import render, redirect

from .models import Artist
from .models import ArtistVideo
from .models import ArtistCollect

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            user.groups.add(Group.objects.get(name='Artist'))
            login(request, user)
            return redirect(reverse_lazy('user:profile'))
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})

# Artist CRUD

class ArtistList(ListView):
    model = Artist

class ArtistDetail(DetailView):
    model = Artist

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        artist_id = context['artist'].id
        genre = context['artist'].genre
        videos  = {}
        related = {}
        try:
            videos  = ArtistVideo.objects.filter(artist=artist_id)
            related = Artist.objects.filter(genre=genre)
        except Artist.DoesNotExist:
            videos  = {}
            related = {}
        context['videos']  = videos
        context['related'] = related
        return context

class ArtistCreate(CreateView):
    model = Artist
    fields = ['name', 'email', 'web', 'phone', 'contact', 'picture', 'description', 'genre']

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(ArtistCreate, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('artists:list')

class ArtistUpdate(UpdateView):
    model = Artist
    success_url = reverse_lazy('artists:list')
    fields = ['name', 'email', 'web', 'phone', 'contact', 'picture', 'description', 'genre']

class ArtistDelete(DeleteView):
    model = Artist
    success_url = reverse_lazy('artists:list')

# ArtistVideo CRUD

class ArtistVideoList(ListView):
    model = ArtistVideo
    template_name = 'artist/artistvideo_list.html'
    context_object_name = 'videos'

    # Filtrar lista por el Foreign Key
    def get_queryset(self):
        return ArtistVideo.objects.filter(artist_id=self.kwargs['fk'])

class ArtistVideoDetail(DetailView):
    model = ArtistVideo

class ArtistVideoCreate(CreateView):
    model = ArtistVideo
    fields = ['url', 'order']

    # Asignar Foreign Key del artista
    def form_valid(self, form):
        artist = Artist.objects.get(pk=self.kwargs['fk'])
        form.instance.artist = artist
        return super(ArtistVideoCreate, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('artists:video_list',
                            kwargs={'fk': self.kwargs['fk']})

class ArtistVideoUpdate(UpdateView):
    model = ArtistVideo
    fields = ['url', 'order']

    def get_success_url(self):
        return reverse_lazy('artists:video_list',
                            kwargs={'fk': self.kwargs['fk']})

class ArtistVideoDelete(DeleteView):
    model = ArtistVideo

    def get_success_url(self):
        return reverse_lazy('artists:video_list',
                            kwargs={'fk': self.kwargs['fk']})

# ArtistCollect CRUD

class ArtistCollectList(ListView):
    model = ArtistCollect
    template_name = 'artist/artistcollect_list.html'
    context_object_name = 'collects'

    # Filtrar lista por el Foreign Key
    def get_queryset(self):
        return ArtistCollect.objects.filter(artist_id=self.kwargs['fk'])

class ArtistCollectDetail(DetailView):
    model = ArtistCollect

class ArtistCollectCreate(CreateView):
    model = ArtistCollect
    fields = ['name', 'total', 'description', 'date']

    # Asignar Foreign Key del artista
    def form_valid(self, form):
        artist = Artist.objects.get(pk=self.kwargs['fk'])
        form.instance.artist = artist
        return super(ArtistCollectCreate, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('artists:collect_list',
                            kwargs={'fk': self.kwargs['fk']})

    def get_context_data(self, **kwargs):
        allow = True
        try:
            opened = ArtistCollect.objects.get(artist_id=self.kwargs['fk'], closed=False)
            if (opened):
                allow = False
        except ArtistCollect.DoesNotExist:
            allow = True
        context = super().get_context_data(**kwargs)
        context['allow_add'] = allow
        return context

class ArtistCollectUpdate(UpdateView):
    model = ArtistCollect
    fields = ['name', 'total', 'description', 'date']

    def get_success_url(self):
        return reverse_lazy('artists:collect_list',
                            kwargs={'fk': self.kwargs['fk']})

class ArtistCollectDelete(DeleteView):
    model = ArtistCollect

    def get_success_url(self):
        return reverse_lazy('artists:collect_list',
                            kwargs={'fk': self.kwargs['fk']})