from django.db import models

# Create your models here.

class Menu(models.Model):
    name 		= models.CharField(max_length=200)
    path 		= models.CharField(max_length=200)
    order       = models.IntegerField()
    disabled    = models.BooleanField()