#menu.py
from django import template
from django.contrib.auth.models import User
from django.template import RequestContext
from artist.models import Artist

register = template.Library()

@register.inclusion_tag('base/menu.html', takes_context=True)
def menu(context):
	return {}

@register.inclusion_tag('base/submenu.html', takes_context=True)
def submenu(context):
	request = context['request']
	name 	= False
	artist  = False
	if request.user.is_authenticated():
		name = request.user
		try:
			artist = Artist.objects.get(user=request.user)
		except Artist.DoesNotExist:
			artist = False
	return {'name' : name, 'artist' : artist}