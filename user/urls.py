from django.conf.urls import url
from . import views

from .views import (
    UserUpdate
)

urlpatterns = [

    url(r'^signup/$', views.signup, name='signup'),
    url(r'^profile$', views.profile, name='profile'),
    url(r'^edit/(?P<pk>\d+)$', UserUpdate.as_view(), name='edit')

]