from django.core.urlresolvers import reverse_lazy

from django.shortcuts import render
from artist.models import Artist
from django.contrib.auth.models import User
from django.views.generic.edit import (
    UpdateView
)
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from django.shortcuts import render, redirect

# Create your views here.

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect(reverse_lazy('user:profile'))
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})

def profile(request):
	if request.user.is_authenticated():
		name = request.user
	else:
		raise ValueError('You are not logged in')
	try:
		artist = Artist.objects.get(user=request.user)
	except Artist.DoesNotExist:
		artist = False
	return render(request, 'profile.html', {'name': name, 'artist': artist})

# User Update 

class UserUpdate(UpdateView):
	model = User
	success_url = reverse_lazy('user:update')
	fields = ['name', 'email', 'password']